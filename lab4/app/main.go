package main

import (
    "fmt"
    "net/http"
    "os"
    "bufio"
)

func readFile(w http.ResponseWriter) {
    file, err := os.Open("/static/filename.txt")
    if err != nil {
        panic(err)
    }
    defer file.Close()

    scanner := bufio.NewScanner(file)
    var text string
    for scanner.Scan() {
        text += scanner.Text()
    }

    if err := scanner.Err(); err != nil {
        panic(err)
    }
    fmt.Fprintf(w, text)
}

func main() {
    port := os.Getenv("PORT")
    if port == "" {
        port = "8080"
    }

    http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
        readFile(w)
    })

    err := http.ListenAndServe(":"+port, nil)
    if err != nil {
        panic(err)
    }
}