## Lab2, CrazyDoctor, d-VorNick

### Build openssl image:
```
docker build -t myopenssl:latest ./openssl/
```

### Build nginx image:
```
docker build -t mynginx:latest .
```

### Run:

```
docker-compose up -d
```

### Voila! localhost:8081