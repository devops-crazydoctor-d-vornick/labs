#!/bin/sh
openssl req -x509 -sha256 -newkey rsa:2048 -nodes -days 30 -keyout /usr/local/cert/key.pem -out /usr/local/cert/cert.pem -subj "/CN=localhost"