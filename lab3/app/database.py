import uuid
from copy import copy

from sqlalchemy import Column, String
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from app import settings

engine = create_async_engine(settings.DB_URL, future=True, echo=False)
async_session = sessionmaker(engine, expire_on_commit=False, class_=AsyncSession)
Base = declarative_base()


async def get_session() -> AsyncSession:
    async with async_session() as session:
        yield session


class Test(Base):
    __tablename__ = "test"

    guid = Column(UUID(as_uuid=True), default=uuid.uuid4, primary_key=True, index=True, unique=True)
    code = Column(String, nullable=True)
    column = Column(String, nullable=True)

    def to_api_dict(self):
        api_dict = copy(self.__dict__)

        api_dict.pop('guid')
        api_dict.pop('parent_guid')
        api_dict.pop('is_deleted')

        api_dict['counter'] = None

        api_dict['id'] = str(self.guid)
        api_dict['parent'] = str(self.parent_guid) if self.parent_guid is not None else None

        return api_dict
