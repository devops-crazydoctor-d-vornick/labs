from fastapi import APIRouter, Depends, Response
from sqlalchemy.ext.asyncio import AsyncSession
from starlette.responses import FileResponse

from app.database import get_session

router = APIRouter()


@router.get(
    "/health/",
    status_code=200,
)
@router.head(
    "/health/",
    status_code=200,
)
async def health_check(
        db: AsyncSession = Depends(get_session),
):
    return Response(content=(await db.execute("SELECT version_num FROM alembic_version")).first()[0], status_code=200)


@router.get(
    "/picture/",
    status_code=200,
)
async def picture(
):
    return FileResponse(path='./static/picture.jpg', status_code=200)
